from dataclasses import dataclass
from datetime import datetime
from json import load, dump
from pathlib import Path
from typing import Union


@dataclass(frozen=True)
class KW:
    """input dict keys and some constants"""
    FIELDS: str = "FIELDS"
    DATABASE: str = "DATABASE"
    TABLE_NAME: str = "TABLE_NAME"
    FROM: str = "FROM"
    WHERE: str = "WHERE"
    WHEN: str = "WHEN"
    GROUP: str = "GROUP"
    ORDER: str = "ORDER"
    DATENAME: str = "DATENAME"
    DATESTART: str = "DATESTART"
    DATEEND: str = "DATEEND"
    DATEFMT: str = "%Y-%m-%d %H:%M:%S"


class QueryBuilder:
    """
    builds queries on single table given the parameters
    
    no check is performed on the SQL correctedness of the
    statement i.e. 'SELECT * FROM some_table GROUP BY some_col' 
    is a valid output of this code, but not a valid query
    """
    
    def __init__(self, params:dict):
        assert isinstance(params, dict)
        self._params = params
        self.body: list[str] = [] # final query text
        
    def _select(self):
        fields = self._params.get(KW.FIELDS, "*")
        cols = self._listify(fields)
        self.body.append("SELECT")
        for f in cols:
            self.body.append(f)
            self.body.append(",")
        self.body[-1] = "\n"
    
    def _from(self):
        db = self._params[KW.DATABASE]
        assert isinstance(db, str)
        table = self._params[KW.TABLE_NAME]
        assert isinstance(table, str)
        self.body.append(f"FROM {db}.{table}\n")
        
    def _where(self):
        where = self._params.get(KW.WHERE, None)
        when = self._params.get(KW.WHEN, None)
        filters = dict()
        (where_len, when_len) = (0, 0)
        if where:
            assert isinstance(where, dict)
            filters.update(where)
            where_len = len(where)
        if when:
            assert isinstance(when, dict)
            filters.update(when)
            when_len = len(when)
        assert len(filters) == where_len + when_len
        if not len(filters):
            return
        conditions = []
        for i, (k, v) in enumerate(filters.items()):
            if isinstance(v, datetime):
                sign = ">=" if k == KW.DATESTART else "<="
                colname = filters[KW.DATENAME]
                date_as_str = v.strftime(KW.DATEFMT) # proper date format
                conditions.append(f"{colname} {sign} '{date_as_str}'")
            elif k == KW.DATENAME:
                continue
            elif isinstance(v, list):
                condition_body = []
                condition_body.append(f"{k} IN (")
                for elem in v:
                    condition_body.append(f"'{elem}'")
                    condition_body.append(",")
                condition_body[-1] = ")"
                conditions.append(" ".join(condition_body))
            elif isinstance(v, str):
                conditions.append(f"{k} = '{v}'")
            else:
                raise TypeError(f"error at key '{k}' in the WHERE section")
        for i, e in enumerate(conditions):
            kw = "WHERE" if not i else "AND"
            self.body.append(f"{kw} {e}\n")
    
    def _group(self):
        groupers = self._params.get(KW.GROUP, None)
        if not groupers:
            return
        g = self._listify(groupers)
        self.body.append("GROUP BY")
        for i in g:
            self.body.append(i)
            self.body.append(",")
        self.body[-1] = "\n"

    def _order(self):
        groupers = self._params.get(KW.ORDER, None)
        if not groupers:
            return
        g = self._listify(groupers)
        self.body.append("ORDER BY")
        for i in g:
            self.body.append(i)
            self.body.append(",")
        self.body[-1] = "\n"
            
    def build_query(self) -> str:
        """builds a basic query by sections"""
        self._select()
        self._from()
        self._where()
        self._group()
        self._order()
        return " ".join(self.body)

    @classmethod
    def fromjson(cls, jsonfile:Path):
        """constructor from external json file"""
        assert isinstance(jsonfile, Path)
        assert jsonfile.exists()
        assert jsonfile.suffix == ".json"
        with jsonfile.open() as f:
            params = load(f)
        # handle the datetime object
        for datekey in (KW.DATESTART, KW.DATEEND):
            try:
                params[KW.WHEN][datekey] = datetime(*params[KW.WHEN][datekey])
            except KeyError:
                pass
        return cls(params)
    
    @staticmethod
    def provide_json():
        "creates an example json file to be used as external constructor"
        file = Path(__file__).with_name("input_template.json")
        example = {KW.FIELDS: "COUNT(*)",
                  KW.DATABASE: "FIRDS",
                  KW.TABLE_NAME: "some_table",
                  KW.WHERE: {"isin_code": "aaa",
                            "another_col": ["12", "34", "56"]},
                  KW.GROUP: ["isin_code", "the_third_col"],
                  KW.ORDER: ["isin_code"],
                  KW.WHEN: {KW.DATENAME: "snapshot_date", 
                            KW.DATESTART: [2020, 1, 31], 
                            KW.DATEEND: [2022, 12, 14]}}
        with file.open("w") as f:
            dump(obj=example, fp=f, indent=4)
        return file
    
    @staticmethod
    def _listify(inpt: Union[str, list[str]]) -> list[str]:
        """ensures output is list of str"""
        if isinstance(inpt, str):
            return [inpt]
        elif isinstance(inpt, list):
            assert all(isinstance(e, str) for e in inpt)
            return inpt
        else:
            raise TypeError()


def build_query(params:Union[dict, Path]) -> str:
    """
    
    builds queries on single tables given the parameters
    
    'params' can be either a dictionary as in the below example or
    a Path object pointing to a json file with a pre-specified 
    structure. in case of errors, an exhaustive example json file
    will be produced and highlighted to the user.
    
    NOTE: no check is performed on the SQL correctedness of the
    statement i.e. 'SELECT * FROM some_table GROUP BY some_col' 
    is a valid output of this code, but not a valid query
    
    ## input example

    params = {"FIELDS": "*",
              "DATABASE": "FIRDS",      
              "TABLE_NAME": "some_table",
              "WHERE": {"ISIN_CODE": ["aaaa", "bbbb", "cccc"],
                        "ANOTHER_COL": ["12", "34", "56"]},
              "GROUP": ["ISIN_CODE", "THE_THIRD_COL"],
              "WHEN":{"DATENAME": "SNAPSHOT_DATE", 
                      "DATESTART": datetime(2020, 1, 31), 
                      "DATEEND": datetime.today()}}    
    
    """
    if isinstance(params, dict):
        return QueryBuilder(params).build_query()
    elif isinstance(params, Path):
        return QueryBuilder.fromjson(params).build_query()
    msg = ("It seems you did not provide good input.",
           "Try having a look at the example here:",
           f"{QueryBuilder.provide_json()}")
    raise TypeError(" ".join(msg))