import asyncio
from os import system
from pathlib import Path
from pprint import pprint
from sqlite3 import Row, connect


class _SQLRow(Row):
    """extension of the sqlite3.Row class"""

    def asdict(self) -> dict:
        return {name:self[name] for name in self.keys()}

    def __repr__(self) -> str:
        return str(self.asdict())


def _query_db(dbpath:Path, query:str) -> list[_SQLRow]:
    """executes the provided query on the inndicated db"""
    with connect(dbpath) as conn:
        conn.row_factory = _SQLRow
        return conn.execute(query).fetchall()


async def async_query_db(dbpath:Path, query:str) -> list[_SQLRow]:
    print(query)
    r =  await asyncio.to_thread(_query_db, dbpath, query)
    print(query, " done")
    return r


async def main():
    _ = system("cls")
    database = Path(__file__).parent / "school.db"

    query1 = "SELECT * FROM school"
    query2 = "SELECT id FROM school"
    query3 = "SELECT * FROM school WHERE grade=8"

    queries = [query1, query2, query3]

    results = await asyncio.gather(*[async_query_db(database, q) for q in queries])

    pprint(results)


if __name__ == "__main__":
    asyncio.run(main())