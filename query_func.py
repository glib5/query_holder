from pathlib import Path
from queue import Queue
from threading import Thread
from typing import Dict, List


def read_queries(filepaths: List[Path]) -> List[str]:
    """
    reads all the queries in the given files

    these must be located in the provided queries_path

    Args:
        filenames (list[Path]): list of files to be read
    returns:
        a list of strings, each one is a query text
    ### notes
    - they must be all .sql files
    - remember to NOT put a semicolon at the end
    """

    assert isinstance(filepaths, list)
    for fp in filepaths:
        assert isinstance(fp, Path)
        assert fp.exists()
        assert fn.suffix == ".sql"

    return [f.read_text() for f in filepaths]
    

def connect():
    """
    must return a connection object
    that supports the creation of cursor objects
    (CX_Oracle, sqlite3, ...)
    """
    raise ConnectionError(f"{self.__class__.__name__} must implement a connect method!")


def _execute(qry: str, conn) -> List[Dict]:
    """
    execute and retrieve a single query

    Args:
        qry (str): valid sql statement
        conn: DB connection
    Returns:
        list of dicts: the query result
    """
    with conn.cursor() as cur:
        cur.execute(qry)
        rows = cur.fetchall()
        columns = [x[0] for x in cur.description]
    return [dict(zip(columns, row)) for row in rows]


def _worker(q: Queue, results: List):
    """This is the worker function for the multithreading approach"""
    with connect() as conn:
        while True:
            (i, qry) = q.get()
            results[i] = self._execute(qry, conn)
            q.task_done()


def query(queries: List[str], threading: bool = False, workers: int = 4) -> List[List[Dict]]:
    """retrieve all the queries in the provided list
    
    Args:
        queries (list[str]): list of sql statements to be executed
        >>> queries = ["SELECT * FROM school", "SELECT MAX(grade) FROM students"]

        threading (bool, default=False): if multithreading or not
        workers (int, default=4): maximum number of threads allowed. must be positive. is capped at 10
    Returns:
        list[list[dict]]: values are list of dicts, where every dict is one row of the query result   
        (same order of the input queries)
    """

    assert isinstance(queries, list)
    assert all(isinstance(q, str) for q in queries)
    assert isinstance(threading, bool)
    assert isinstance(workers, int)
    assert workers > 0
    # never more than MAXSIZE threads
    MAXSIZE = 10
    _max_workers = min(MAXSIZE, workers)
    final = list(range(len(queries)))
    if threading: # use threads, each with his own connection
        q = Queue()
        for iqry in enumerate(queries):
            q.put(iqry)
        for _ in range(_max_workers):
            Thread(target=_worker, args=(q, final), daemon=True).start()
        q.join()
    else: # one connection, many queries
        with connect() as conn:
            for i, qry in enumerate(queries):
                final[i] = _execute(qry, conn)
    return final
