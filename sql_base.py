from contextlib import contextmanager
from pathlib import Path
from sqlite3 import Connection, Cursor, OperationalError, Row, connect


class SQLRow(Row):
    """extension of the sqlite3.Row class"""

    def asdict(self) -> dict:
        return {name:self[name] for name in self.keys()}

    def __repr__(self) -> str:
        body = [f"{self.__class__.__name__}("]
        for name in self.keys():
            body.append(f"{name}={self[name]}")
            body.append(", ")
        body[-1] = ")"
        return "".join(body)


@contextmanager
def cursor(conn:Connection):
    """turns the cursor object into a contextmanager"""
    try:
        cur = Cursor(conn)
        yield cur
    finally:
        cur.close()


def query_db(dbpath:Path, query:str):
    """executes the provided query on the inndicated db"""
    with connect(dbpath) as conn:
        conn.row_factory = SQLRow
        return conn.execute(query).fetchall()


def dump_db(database:Path, dumpfile:Path) -> Path:
    """dump a db to a .sql file"""
    with connect(database) as conn:
        dumpfile.write_text("\n".join(conn.iterdump()))


def restore_db(dumpfile:Path, newfile) -> Path:
    """from a dump.sql file, create a .db file"""
    try:
        instructions = dumpfile.read_text()
        with connect(newfile) as conn:
            with cursor(conn) as cur:
                for line in instructions.split(";"):
                    cur.execute(line)
    except OperationalError:
        pass # table already exists

# ============================================================================

def main():
    from os import system
    from pprint import pprint
    _ = system("cls")

    # define a db, somehow
    database = Path(__file__).parent / "school.db"
    query = "SELECT * FROM school"

    # query data from it
    data:list[SQLRow] = query_db(database, query)

    # Convert file school.db to SQL dump file dump.sql
    dumpfile = database.with_name(f"dump_{database.stem}.sql")
    dump_db(database, dumpfile)

    # restore the data from the dump.sql instructions
    restored_db = dumpfile.with_name(f"restored_from_{dumpfile.stem}.db")
    restore_db(dumpfile, restored_db)

    print(database, "\n")
    print(dumpfile, "\n")
    print(restored_db, "\n")
    pprint(data)
    print()
    data.sort(key=lambda x: x["major"])
    pprint([d.asdict() for d in data])
    print()


if __name__ == "__main__":
    main() 