# SQL world

Variuos Object and function to interact with SQL queries and their results.

## queryreader

abc for easy querying with a specific connection

``` python

from qyeryreader import QueryReader

# create a subclass

import sqlite3

class SchoolQuerier(QueryReader):

    def connect(self):
        return sqlite3.connect("school.db")



# execute many queries
queries = ["SELECT * FROM school", "SELECT DICTINCT(subject) FROM school"]

school_querier = SchoolQuerier()

data = school_querier.query(queries)

# for dataframes
import pandas as pd

df = [pd.DataFrame(d) for d in data]

```
